const acciones = require('./activacionMaxima.json')

module.exports = main

function main(send){

  const DELAY_TIME = 150
  var interval = null

  //ACTIVAR
  function activar() {
    var atAccion = 0
    interval = setInterval(nextAccion,DELAY_TIME)
    function nextAccion(){
      if(atAccion < acciones.length){
        var acc = acciones[atAccion]
        var obj = { "n_accion": acc["nAccion"], "valor": acc["param"]}
        console.log(obj)
        send(obj)
        atAccion ++
      }else{
        clearInterval(interval) 
      }
    }
  }
  //DESACTIVAR
  function desactivar(){
    if(interval){
      clearInterval(interval)
    }
  }

  function apagar(){
    var atAccion = 0
    var atPisoTerraza = 4
    interval = setInterval(nextAccion, DELAY_TIME)
    function nextAccion(){
      if(atAccion < acciones.length){
        var acc = acciones[atAccion]
        var obj
        if(acc["nAccion"] === 30){
          var obj = {"n_accion": acc["nAccion"], "valor":atPisoTerraza}
          atPisoTerraza++
        }else if(acc["nAccion"] === 53){
          var obj = {"n_accion": acc["nAccion"], "valor":30}
	}else{
          var obj = {"n_accion": acc["nAccion"], "valor":0}
        }
        send(obj)
        atAccion++
      }else{
        clearInterval(interval)
      }
    }
  }

  return {
    activar: activar,
    desactivar: desactivar,
    apagar: apagar
  }
}



