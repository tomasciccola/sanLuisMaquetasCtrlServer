const tcp = require('net')
const streamSet = require('stream-set')
const log = require('./logUtils.js')

module.exports = function (P) {
  const PORT = P || 9001
  var conexiones = streamSet()

  initServer(PORT)

  return sendMsgToHardware

  function sendMsgToHardware (msg) {

    var msgStruct = {
      HEADER: Buffer.from('~#'),
      TIPO: Buffer.alloc(1),
      N_ACCION: Buffer.alloc(1),
      VALOR: Buffer.alloc(1),
      FOOTER: Buffer.from('\n\r')
    }
    msgStruct['TIPO'].writeUInt8(1)// por ahora el tipo va a ser siempre 1
    msgStruct['N_ACCION'].writeUInt8(msg['n_accion'])
    msgStruct['VALOR'].writeUInt8(msg['valor'])
    // Concateno cada elemento en un array
    var arr = [
      msgStruct['HEADER'],
      msgStruct['TIPO'],
      msgStruct['N_ACCION'],
      msgStruct['VALOR'],
      msgStruct['FOOTER']
    ]

    // Meto todo en un buffer
    var buffer = Buffer.concat(arr)

    /*
    var header = "~#"
    var footer = "\n\r"
    var type = 1
    var buffer = Buffer.from(header + type + msg['n_accion'] + msg['valor'] + footer)
    */

    console.log(`${log.getTimestamp()} >> mandando msg a arduinx(s):`, buffer)
    conexiones.forEach(sendMsg)

    function sendMsg (conn) {
      conn.write(buffer)
    }
  }

  function initServer (port) {
    var server = tcp.createServer(onConnection)

    function onConnection (socket) {
      console.log(`${log.getTimestamp()} >> nueva conexión de ${socket.remoteAddress.split(":")[3]}`)
      conexiones.add(socket)

      socket.on('data', (data) => {
      // Repeticelo a todas la conexiones
        conexiones.forEach((conexion) => {
        // Si no es vos misma :P
          if (conexion !== socket) {
            conexion.write(data)
          }
        })
      })

      socket.on('close', function () {
      console.log(`${log.getTimestamp()} >> conexión cerrada`)
        setTimeout(() => {
          console.log(`${log.getTimestamp()} >> n conexiones: ${conexiones.size}`)
        }, 1000)

      })
    }

    server.listen(port)
    console.log(`${log.getTimestamp()} >> Servidor arduino escuchando en puerto ${port}`)
  }
}
