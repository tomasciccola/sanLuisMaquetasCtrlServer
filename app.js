#!/usr/bin/node

const http = require('http')
const Router = require('routes-router')
const sendJson = require('send-data/json')
const log = require('./logUtils.js')

const errHandlers = require('./errHandlers.js')
const msgConvert = require('./webApiToHwMsg.js')
const toHw = require('./hwCtrl.js')(9001)

const bot = require('./botMode.js')(toHw)
const activadorMaximo = require('./activadorMaximo.js')(toHw)

const PORT = 8001 || process.argv[2]


var ctrlMode = false
var botMode = false
var modoActivacion = false

var app = Router({
  notFound: errHandlers.notFound,
  errorHandler: errHandlers.err
})

app.addRoute('/accionMaqueta/:modulo/:zona/:efecto/:control', function (req, res, opts) {
  res.setHeader('Access-Control-Allow-Origin', '*');
	res.setHeader('Access-Control-Request-Method', '*');
	res.setHeader('Access-Control-Allow-Methods', 'OPTIONS, GET');
	res.setHeader('Access-Control-Allow-Headers', '*');

  if(!ctrlMode){
    handleRequest(opts, function(err){
      if(err){
        return sendJson(req,res,{
          statusCode: 200,
          body: { msg: 'ERROR. Algún parámetro del GET puede estár mal' }
        })
      }else{
        // Mando status message a la web
        return sendJson(req, res, {
          statusCode: 200,
          body: { msg: 'OK' }
        })
      }

    })
  }else{
    // No podes controlar porque esta el modo de control activado
    return sendJson(req, res, {
      statusCode: 200,
      body: { msg: 'UNAVAILABLE' }
    })
  }
})

//Hago una API levemente distinta para diferencias la web y la web interna sin tener que ver el origin
app.addRoute('/dev/accionMaqueta/:modulo/:zona/:efecto/:control', function (req, res, opts) {
  res.setHeader('Access-Control-Allow-Origin', '*');
	res.setHeader('Access-Control-Request-Method', '*');
	res.setHeader('Access-Control-Allow-Methods', 'OPTIONS, GET');
	res.setHeader('Access-Control-Allow-Headers', '*');

  if(ctrlMode){
    handleRequest(opts, function(err){
      if(err){
        return sendJson(req,res,{
          statusCode: 200,
          body: { msg: 'ERROR. Algún parámetro del GET puede estár mal' }
        })
      }else{
        // Mando status message a la web
        return sendJson(req, res, {
          statusCode: 200,
          body: { msg: 'OK' }
        })
      }

    })
  }else{
    return sendJson(req, res, {
      statusCode: 200,
      body: { msg: 'No estás en modo de control :(' }
    })
    
  }

})

app.addRoute('/modoInterno/:val', function(req,res,opts){
  res.setHeader('Access-Control-Allow-Origin', '*');
	res.setHeader('Access-Control-Request-Method', '*');
	res.setHeader('Access-Control-Allow-Methods', 'OPTIONS, GET');
	res.setHeader('Access-Control-Allow-Headers', '*');

  ctrlMode = opts.params.val === '1' ? true : false
  if(ctrlMode){
    console.log(`${log.getTimestamp()} >> modo de control interno activado`)
    return sendJson(req,res,{
      statusCode:200,
      body: {msg:'modo de control interno activado'}
    })
  }else{
    console.log(`${log.getTimestamp()} >> modo de control interno desactivado`)
    return sendJson(req,res,{
      statusCode:200,
      body: {msg:'modo de control interno desactivado'}
    })
  }
})


app.addRoute('/modoAutomatico/:val', function(req,res,opts){
  res.setHeader('Access-Control-Allow-Origin', '*');
	res.setHeader('Access-Control-Request-Method', '*');
	res.setHeader('Access-Control-Allow-Methods', 'OPTIONS, GET');
	res.setHeader('Access-Control-Allow-Headers', '*');
  botMode = opts.params.val === '1' ? true : false
  if(botMode){
    console.log(`${log.getTimestamp()} >> modo de control automático activado`)
    return sendJson(req,res,{
      statusCode:200,
      body: {msg:'modo bot activado'}
    })
    bot.activar()
  }else{
    console.log(`${log.getTimestamp()} >> modo de control automático desactivado`)
    return sendJson(req,res,{
      statusCode:200,
      body: {msg:'modo bot desactivado'}
    })
    bot.desactivar()
  }
})

app.addRoute('/modoActivacion/:val', function(req,res, opts){
  res.setHeader('Access-Control-Allow-Origin', '*');
	res.setHeader('Access-Control-Request-Method', '*');
	res.setHeader('Access-Control-Allow-Methods', 'OPTIONS, GET');
	res.setHeader('Access-Control-Allow-Headers', '*');
  modoActivacion = opts.params.val === '1' ? true : false
  if(modoActivacion){
    console.log(`${log.getTimestamp()} >> modo de activacion máxima activado`)
    activadorMaximo.activar()
    return sendJson(req,res,{
      statusCode:200,
      body: {msg:'modo activacion activado'}
    })
    console.log("HOLA")
  }else{
    console.log(`${log.getTimestamp()} >> modo de activacion máxima desactivado`)
    activadorMaximo.apagar()
    return sendJson(req,res,{
      statusCode:200,
      body: {msg:'modo activacion desactivado'}
    })
  }
})

function handleRequest(opts, cb){
  console.log(`${log.getTimestamp()} >> nueva accion: ${JSON.stringify(opts.params)}`)
  msgConvert(opts.params, function(err,data){
    if(err) return cb(err, null)
    console.log(`${log.getTimestamp()} >> mensaje para arduino: ${JSON.stringify(data)}`)
    toHw(data)
    return cb(null)
  })
}

var server = http.createServer(app)
server.listen(PORT)

console.log(`${log.getTimestamp()} >> Api escuchando en ${PORT}`)
