# Control maqueta San Luis

Levanta un server http que funciona como web api y levanta un server tcp para mandarle los mensajes de control a los arduinos

## Schema arduino (PUERTO:9001)

* HEADER : "~#" --> 2 bytes 

* TIPO :  "" --> 1 byte {0 -> mesnaje entre arduino 1-> msg server a client, 2--> mensaje client a server}

* N_ACCION : "" --> 1 byte

* VAL : "" --> n bytes

* FOOTER: "/n/r"


## Api web (PUERTO:8001)
/accionMaqueta/:modulo/:zona/:efecto/:control

devuelve --> {"msg":"OK"} 

             {"msg":"UNAVAILABLE"} --> en el caso del modo demo

