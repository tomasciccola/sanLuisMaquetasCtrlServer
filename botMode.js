const schema = require('./schemaZonas.json')

module.exports = function(send,i){
  const TIME = 1000 || i
  const REL_TIME = 3000
  var interval;

  
  function activar(){
    interval = setInterval(activarAccionRandom, TIME)
  }

  function desactivar(){
    clearInterval(interval)
  }

  return {
    activar:activar,
    desactivar:desactivar
  }

  function activarAccionRandom(){
    //Random modulo
    var rndModulo = getRandKeyObj(schema)
    //Random zona
    var rndZona = getRandKeyObj(rndModulo)
    //Random efecto
    var rndEfecto = getRandKeyObj(rndZona)
    console.log(rndEfecto)

    var obj = { "n_accion":rndEfecto['nAccion'], "valor":null }

    if(rndEfecto["slider"]){
      obj["valor"] = Math.floor(Math.random() * rndEfecto["slider"])
    }else{
      obj["valor"] = 1
    }

    send(obj)

    setTimeout(function(){
      obj["valor"] = 0
      send(obj)
    }, REL_TIME)

    function getRandKeyObj(obj){
      var keys = Object.keys(obj)
      return obj[keys[Math.floor(Math.random() * keys.length)]]
    }
  }
}

