const sendJson = require('send-data/json')

module.exports.notFound = function (req, res) {
  sendJson(req, res, {
    statusCode: 404,
    body: { error: 'Ruta no encontrada' }
  })
}

module.exports.err = function (req, res, err) {
  sendJson(req, res, {
    statusCode: err.statusCode || 500,
    body: { error: err.message }
  })
}
